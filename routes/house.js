const express = require("express");
const con = require("../db/connection");
const houseRouter = express.Router();

houseRouter.get("/get-all", async (req, res) => {
  try {
    con.query(
      "SELECT * FROM house inner join area on house.houseid = area.areaid inner join roof on house.houseid = roof.roofid inner join sale on house.houseid = sale.houseid inner join utilities on house.houseid = utilities.utilitiid inner join basement on house.houseid = basement.basementid inner join lot on house.houseid = lot.lotid inner join exterior on house.houseid = exterior.exteriorid inner join fireplace on house.houseid = fireplace.fireplaceid inner join kitchen on house.houseid = kitchen.kitchenid inner join garage on house.houseid = garage.garageid inner join miscellaneous on house.houseid  = miscellaneous.miscid;",
      function (err, result, fields) {
        if (err) throw err;
        res.status(200).send(result);
      }
    );
  } catch (error) {
    res.status(500).send(error);
  }
});

houseRouter.post("/update", async (req, res) => {
  try {
    var queryParameter = req.body;
    var errorMessage = "";
    for (const table in queryParameter) {
      var updatequery = "UPDATE ";
      
      if (queryParameter[table]["id"] === "" || queryParameter[table]["id"] === undefined) {
        errorMessage += "Id for table " + table + " is mandatory. \n";
      } else {
        const keys = Object.keys(queryParameter[table]).filter(
          (key) => key !== "id"
        );
        var flag = 0
        updatequery += table + " SET ";
        for (const column in queryParameter[table]) {
          if (queryParameter[table][column] !== "" && column !== "id") {
            console.log(column,queryParameter[table][column])
            flag = 1
            if (keys[keys.length - 1] === column) {
              updatequery += column + " = " + queryParameter[table][column];
            } else {
              updatequery +=
                column + " = " + queryParameter[table][column] + ", ";
            }
            
          }
        }
        updatequery += " where id = " + queryParameter[table]["id"] + ";";
        console.log(updatequery)
        if(flag == 1){
          await con.query(updatequery, function (err, result, fields) {
            if (err) throw err;
          });
        }
        else{
          res.send({errMsg : "Atleast one field should update!"})
        }
      }
    }
    if(errorMessage !== ""){
      res.send({errorMessage})
    }
    else{
      req.send({successMsg:"Tables updated successfully!."})
    }

  } catch (err) {
    res.status(500).send(err.message);
  }
});

houseRouter.post("/add", async (req, res) => {
  try {
    var queryParameter = req.body;
    for (const table in queryParameter) {
      var addquery = "INSERT INTO " +table+" ( ";
      const keys = Object.keys(queryParameter[table]);
      for (const column in queryParameter[table]) {
        if (keys[keys.length - 1] === column){
            addquery+=column+" )"
        }
        else{
            addquery+=column+", "
        }
      }
      addquery+=" VALUES ( "
      for (const column in queryParameter[table]) {
        if (keys[keys.length - 1] === column){
          if(typeof queryParameter[table][column] === 'string'){
            addquery+="'"+queryParameter[table][column]+"'"+" );"
            
          }else{
            addquery+=queryParameter[table][column]+" );"
          }
        }
        else{
          if(typeof queryParameter[table][column] === 'string'){
            addquery+="'"+queryParameter[table][column]+"'"+", "
            
          }else{
            addquery+=queryParameter[table][column]+", "
          }
        }
      }
    }
    console.log(addquery)

      await con.query(addquery, function (err, result, fields) {
        if (err) throw err;
      //res.status(200).send(result);
      res.send({successMsg:"Tables inserted successfully!."})
      });

  } catch (err) {
    //res.status(500).send(err);
    console.log(err.message)
  }
});

houseRouter.post("/filter", async (req, res) => {
  try {
    var queryParameter = req.body;
    if (Object.keys(queryParameter).length !== 0) {
      var query = "select ";
      var flag = 0;
      const tableKeys = Object.keys(queryParameter);
      for (const table in queryParameter) {
        const keys = Object.keys(queryParameter[table]);

        for (const column in queryParameter[table]) {
          if (queryParameter[table][column] !== "") {
            flag = 1;
          }
          if (
            keys[keys.length - 1] === column &&
            tableKeys[tableKeys.length - 1] === table
          ) {
            query += table + "." + column;
          } else {
            query += table + "." + column + ", ";
          }
        }
      }
      query += " from ";
      for (const table in queryParameter) {
        if (tableKeys[tableKeys.length - 1] === table) {
          if (flag === 1) {
            query += table + " ";
          } else {
            query += table + ";";
          }
        } else {
          if (tableKeys.length === 1) {
            if (flag === 1) {
              query += table + " ";
            } else {
              query += table + ";";
            }
          } else {
            query += table + " natural join ";
          }
        }
      }
      if (flag == 1) {
        query += "where ";
        let subquery = "";

        for (const table in queryParameter) {
          const keys = Object.keys(queryParameter[table]);
          
          for (const column in queryParameter[table]) {
            const nextKey = keys.indexOf(column)+1
            if (queryParameter[table][column] == "") {
            } else {
              if (queryParameter[table][column][0] == "=") {
                subquery +=
                  table +
                  "." +
                  column +
                  " = " +
                  '"' +
                  queryParameter[table][column].substring(1) +
                  '"';
                } else if (
                  queryParameter[table][column][0] == ">" &&
                  queryParameter[table][column][1] == "="
                  ) {
                    subquery +=
                    table +
                    "." +
                    column +
                    " >= " +
                    '"' +
                    queryParameter[table][column].substring(2) +
                    '"';
                  } else if (
                    queryParameter[table][column][0] == "<" &&
                    queryParameter[table][column][1] == "="
                    ) {
                      subquery +=
                      table +
                      "." +
                      column +
                      " <= " +
                      '"' +
                      queryParameter[table][column].substring(2) +
                  '"';
              } else if (queryParameter[table][column][0] == ">") {
                subquery +=
                table +
                  "." +
                  column +
                  " > " +
                  '"' +
                  queryParameter[table][column].substring(1) +
                  '"';
                } else if (queryParameter[table][column][0] == "<") {
                  subquery +=
                  table +
                  "." +
                  column +
                  " < " +
                  '"' +
                  queryParameter[table][column].substring(1) +
                  '"';
                }
                if (
                keys[keys.length - 1] === column &&
                tableKeys[tableKeys.length - 1] === table 
              ) {
                subquery += " ";
              } else {
                if(queryParameter[table][keys[nextKey]] !== "")
                {subquery += " AND ";}
              else{
                {subquery += " ";}
              }              
            }
            }
          }
        }
        query += subquery + ";";
      }
     
      await con.query(query, function (err, result, fields) {
          if (err) throw err;
        //res.status(200).send(result);
        res.status(200).send(result);
      });
    }
  } catch (error) {
    res.status(500).send(error.message);
  }
});

houseRouter.get("/get-house-column", async (req, res) => {
  try {
    con.query(
      "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'house';",
      function (err, result, fields) {
        if (err) throw err;
        res.status(200).send({ table: "House", columns: result });
      }
    );
  } catch (error) {
    res.status(500).send(error);
  }
});

houseRouter.get("/get-area-column", async (req, res) => {
  try {
    con.query(
      "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'area';",
      function (err, result, fields) {
        if (err) throw err;
        res.status(200).send({ table: "Area", columns: result });
      }
    );
  } catch (error) {
    res.status(500).send(error);
  }
});

houseRouter.get("/get-roof-column", async (req, res) => {
  try {
    con.query(
      "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'roof';",
      function (err, result, fields) {
        if (err) throw err;
        res.status(200).send({ table: "Roof", columns: result });
      }
    );
  } catch (error) {
    res.status(500).send(error);
  }
});

houseRouter.get("/get-lot-column", async (req, res) => {
  try {
    con.query(
      "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'lot';",
      function (err, result, fields) {
        if (err) throw err;
        res.status(200).send({ table: "Lot", columns: result });
      }
    );
  } catch (error) {
    res.status(500).send(error);
  }
});

houseRouter.get("/get-kitchen-column", async (req, res) => {
  try {
    con.query(
      "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'kitchen';",
      function (err, result, fields) {
        if (err) throw err;
        res.status(200).send({ table: "Kitchen", columns: result });
      }
    );
  } catch (error) {
    res.status(500).send(error);
  }
});

houseRouter.get("/get-garage-column", async (req, res) => {
  try {
    con.query(
      "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'garage';",
      function (err, result, fields) {
        if (err) throw err;
        res.status(200).send({ table: "Garage", columns: result });
      }
    );
  } catch (error) {
    res.status(500).send(error);
  }
});

houseRouter.get("/get-fireplace-column", async (req, res) => {
  try {
    con.query(
      "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'fireplace';",
      function (err, result, fields) {
        if (err) throw err;
        res.status(200).send({ table: "Fireplace", columns: result });
      }
    );
  } catch (error) {
    res.status(500).send(error);
  }
});

houseRouter.get("/get-exterior-column", async (req, res) => {
  try {
    con.query(
      "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'exterior';",
      function (err, result, fields) {
        if (err) throw err;
        res.status(200).send({ table: "Exterior", columns: result });
      }
    );
  } catch (error) {
    res.status(500).send(error);
  }
});

houseRouter.get("/get-basement-column", async (req, res) => {
  try {
    con.query(
      "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'basement';",
      function (err, result, fields) {
        if (err) throw err;
        res.status(200).send({ table: "Basement", columns: result });
      }
    );
  } catch (error) {
    res.status(500).send(error);
  }
});

houseRouter.get("/get-misc-column", async (req, res) => {
  try {
    con.query(
      "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'miscellaneous';",
      function (err, result, fields) {
        if (err) throw err;
        res.status(200).send({ table: "Miscellanous", columns: result });
      }
    );
  } catch (error) {
    res.status(500).send(error);
  }
});

houseRouter.get("/get-utiliti-column", async (req, res) => {
  try {
    con.query(
      "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'utilities';",
      function (err, result, fields) {
        if (err) throw err;
        res.status(200).send({ table: "Utilities", columns: result });
      }
    );
  } catch (error) {
    res.status(500).send(error);
  }
});

houseRouter.get("/get-sale-column", async (req, res) => {
  try {
    con.query(
      "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'sale';",
      function (err, result, fields) {
        if (err) throw err;
        res.status(200).send({ table: "Sale", columns: result });
      }
    );
  } catch (error) {
    res.status(500).send(error);
  }
});

houseRouter.get("/get-mszoning", async (req, res) => {
  try {
    con.query(
      "SELECT distinct mszoning FROM house",
      function (err, result, fields) {
        if (err) throw err;
        res.status(200).send(result);
      }
    );
  } catch (error) {
    res.status(500).send(error);
  }
});

module.exports = houseRouter;
