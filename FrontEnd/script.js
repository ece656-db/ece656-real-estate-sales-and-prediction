$(document).ready(async function () {
  // Define the table and column values as an array of objects
  var data = [];

  await $.get("http://localhost:3001/get-house-column", function (result) {
    data.push(result);
  });

  await $.get("http://localhost:3001/get-area-column", function (result) {
    data.push(result);
  });

  await $.get("http://localhost:3001/get-roof-column", function (result) {
    data.push(result);
  });

  await $.get("http://localhost:3001/get-lot-column", function (result) {
    data.push(result);
  });

  await $.get("http://localhost:3001/get-kitchen-column", function (result) {
    data.push(result);
  });

  await $.get("http://localhost:3001/get-garage-column", function (result) {
    data.push(result);
  });

  await $.get("http://localhost:3001/get-fireplace-column", function (result) {
    data.push(result);
  });

  await $.get("http://localhost:3001/get-exterior-column", function (result) {
    data.push(result);
  });

  await $.get("http://localhost:3001/get-basement-column", function (result) {
    data.push(result);
  });

  await $.get("http://localhost:3001/get-misc-column", function (result) {
    data.push(result);
  });

  await $.get("http://localhost:3001/get-utiliti-column", function (result) {
    data.push(result);
  });

  await $.get("http://localhost:3001/get-sale-column", function (result) {
    data.push(result);
  });

  var selectedValues = {}; // Initialize an empty object to store the selected values

  // Loop through the data array and dynamically generate the dropdown menu items
  for (var i = 0; i < data.length; i++) {
    var table = data[i].table;
    var columns = data[i].columns;

    var tableItem = $("<li></li>").addClass("dropdown-submenu");
    var tableLink = $("<a></a>")
      .addClass("dropdown-item")
      .attr("href", "#")
      .text(table);
    tableItem.append(tableLink);

    var columnList = $("<ul></ul>").addClass("dropdown-menu");
    for (var j = 0; j < columns.length; j++) {
      var column = columns[j].COLUMN_NAME;
      var columnItem = $("<li></li>");
      var checkbox = $("<input>").attr({ type: "checkbox", value: column });
      columnItem.append(checkbox);
      columnItem.append(" " + column);
      columnList.append(columnItem);

      checkbox.change(function () {
        if ($(this).is(":checked")) {
          var input = $("<input>").attr({ type: "text", maxlength: 50 });
          var inputContainer = $("<div></div>")
            .addClass("input-container")
            .append(input);
          $(this).parent().append(inputContainer);
          // Add the selected values to the object
          var tableValue = $(this)
            .closest(".dropdown-submenu")
            .find(".dropdown-item")
            .text();
          var columnValue = $(this).val();
          selectedValues[tableValue] = selectedValues[tableValue] || {}; // Initialize an empty object for the table if not already present
          selectedValues[tableValue][columnValue] = ""; // Initialize the value of the column to an empty string
          input.on("input", function () {
            selectedValues[tableValue][columnValue] = $(this).val(); // Update the value of the column when the input text changes
          });
        } else {
          $(this).siblings(".input-container").remove();
          // Remove the corresponding values from the object when a column is unchecked
          var tableValue = $(this)
            .closest(".dropdown-submenu")
            .find(".dropdown-item")
            .text();
          var columnValue = $(this).val();
          delete selectedValues[tableValue][columnValue];
          if ($.isEmptyObject(selectedValues[tableValue])) {
            delete selectedValues[tableValue];
          }
        }
        // Log the selected values object to the console for testing purposes
      });
    }
    tableItem.append(columnList);
    $("#table-dropdown").append(tableItem);
  }

  

  // Function to display table data
  function displayData(data, page = 1, rowsPerPage = 10) {
    const tableHead = document.querySelector("#myTableHead");
    const tableBody = document.querySelector("#myTableBody");
    const paginationContainer = document.querySelector("#pagination");
    const start = (page - 1) * rowsPerPage;
    const end = start + rowsPerPage;
    const paginatedData = data.slice(start, end);

    // Clear table body
    tableBody.innerHTML = "";

    // Create header row
    if (!tableHead.querySelector('tr')) {
    const headerRow = document.createElement("tr");
    for (const key in data[0]) {
      const th = document.createElement("th");
      th.textContent = key;
      headerRow.appendChild(th);
    }
    tableHead.appendChild(headerRow);
  }
    // Create data rows
    paginatedData.forEach((rowData) => {
      const row = document.createElement("tr");
      for (const key in rowData) {
        const cell = document.createElement("td");
        cell.textContent = rowData[key] ?? "N/A"; // Display 'N/A' if value is null or undefined
        row.appendChild(cell);
      }
      tableBody.appendChild(row);
    });

    // Create pagination links
    const numPages = Math.ceil(data.length / rowsPerPage);
    paginationContainer.innerHTML = "";
    for (let i = 1; i <= numPages; i++) {
      const link = document.createElement("a");
      link.href = "#";
      link.textContent = i;
      if (i === page) {
        link.classList.add("active");
      }
      link.addEventListener("click", (e) => {
        e.preventDefault();
        displayData(data, i, rowsPerPage);
      });
      paginationContainer.appendChild(link);
    }
  }

  $(document).on("click", "#submit-btn", function () {
    //Send a POST request to the server with the selected values
    $.ajax({
      url: "http://localhost:3001/filter",
      type: "POST",
      data: JSON.stringify(selectedValues),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function (response) {
        displayData(response);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log("Error: " + errorThrown);
      },
    });
  });

  $(document).on("click", "#submit-btn3", function () {
      
  });

  $(document).on("click", "#submit-btn2", function () {
    $.ajax({
      url: "http://localhost:3001/add",
      type: "POST",
      data: JSON.stringify(selectedValues),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function (response) {
        console.log(response);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log("Error: " + errorThrown);
      },
    });
  });


  $(document).on("click", "#submit-btn1", function () {
    //Send a POST request to the server with the selected values
    $.ajax({
      url: "http://localhost:3001/update",
      type: "POST",
      data: JSON.stringify(selectedValues),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function (response) {
        console.log(response);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log("Error: " + errorThrown);
      },
    });
  });
});
