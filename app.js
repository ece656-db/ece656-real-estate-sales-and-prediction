const express = require("express");
const bodyParser = require("body-parser");;
const cors = require("cors");
const houseRouter = require("./routes/house")

const app = express();
app.use(express.json());
// for encoding the URL
app.use(
	express.urlencoded({
		extended: true,
	})
);
app.use(
	bodyParser.urlencoded({
		extended: true,
	})
);

app.use(cors());

app.use(houseRouter);

module.exports = app


