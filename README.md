# ECE656-REAL ESTATE SALES AND PREDICTION

A project for brokers to manage the sale data. A data mining module for prediction has been incorporated as well.

## Getting started

Step 1: User must download all the files and run the following command in terminal

```
npm run install

```
Step 2: Connect to the data base server and Run the file "Query_For Table final.sql" to create the necessary database. 

Step 3: Run the command in the terminal to start the backend

```
npm run start
```
Step 4: open index.html to view the UI.



## UI User guide:

The user can select different tables and columns to filter, add or update records.

### Some rules to be kept in mind:
- For update, entering the ID is compulsory
- For filtering , values should be enetered in the form >/</>=/<=/= 'value'
- For adding values, the data constraints must be kept in mind and appropriate values must be added. 
  - For eg: while inserting a new element, user should enter an ID which is unique and does not already exist, or while entering values for condition, it must be in the appropriate domain.







